
$path=((Get-Item .).FullName)

# start the pod and service
kubectl apply -f $path\out\web-service.yaml,$path\out\web-deployment.yaml
Start-Sleep -s 5
$pod=$(kubectl get pod |Where-Object{$_ -match 'web*'} | ForEach-Object{($_ -split "\s+")[0]})
kubectl port-forward pod/${pod} 80:5000
