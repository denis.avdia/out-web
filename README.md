# out-web



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/denis.avdia/out-web.git
git branch -M main
git push -uf origin main
```



This project is executable in windows docker-desktop installed.

We have 2 different unix timestamp, first is executed when the image is build, and the second when the pod is activated.

Before running the 2 powershel scripts, we have to activate "remotesign" in powershell

1. #Open "powershell.exe" & run

"Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine"


1. #Clone the repo

git clone https://gitlab.com/denis.avdia/out-web.git

2. #Build the images and register the time_stamp

./build.ps1

3. #Apply kubectl for activating

./deploy.ps1

4. #open a new powershell session and run followin commands to test

 $Webrequest = Invoke-WebRequest -Uri http://localhost -UseBasicParsing
 
 $Webrequest.Content

